package ru.t1.aksenova.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class DataJsonLoadFasterXmlRequest extends AbstractUserRequest {

    public DataJsonLoadFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
