package ru.t1.aksenova.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.service.ITokenService;

@Getter
@Setter
@NoArgsConstructor
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
