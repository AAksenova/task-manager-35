package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
