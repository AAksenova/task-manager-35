package ru.t1.aksenova.tm.api.service;

import ru.t1.aksenova.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
