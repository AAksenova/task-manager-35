package ru.t1.aksenova.tm.api.repository;

import ru.t1.aksenova.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
