package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.service.IProjectService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.repository.ProjectRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @Before
    public void before() {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> service.add(NULL_PROJECT));
        Assert.assertNotNull(service.add(ADMIN_PROJECT1));
        @Nullable final Project project = service.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project, service.findOneById(project.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, ADMIN_PROJECT1));
        Assert.assertThrows(AbstractException.class, () -> service.add("", ADMIN_PROJECT1));
        Assert.assertThrows(AbstractException.class, () -> service.add(ADMIN_TEST.getId(), NULL_PROJECT));
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_PROJECT1));
        @Nullable final Project project = service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void createByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.create(null, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(ADMIN_TEST.getId(), null, ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), null));
        @NotNull final Project project = service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(project, service.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void updateByUserIdById() {
        Assert.assertThrows(AbstractException.class, () -> service.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), USER_PROJECT1.getName(), null));
        @NotNull final Project project = service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), PROJECT_NAME, PROJECT_DESCR);
        Assert.assertEquals(project, service.findOneById(USER_TEST.getId(), project.getId()));
        Assert.assertEquals(PROJECT_NAME, project.getName());
        Assert.assertEquals(PROJECT_DESCR, project.getDescription());
        Assert.assertEquals(USER_TEST.getId(), project.getUserId());
    }

    @Test
    public void updateByUserIdByIndex() {
        final int index = repository.findAll().indexOf(USER_PROJECT1);
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(null, index, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), -1, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), index, null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), index, USER_PROJECT1.getName(), null));
        @NotNull final Project project = service.updateByIndex(USER_TEST.getId(), index, PROJECT_NAME, PROJECT_DESCR);
        Assert.assertEquals(project, service.findOneByIndex(USER_TEST.getId(), index));
        Assert.assertEquals(PROJECT_NAME, project.getName());
        Assert.assertEquals(PROJECT_DESCR, project.getDescription());
        Assert.assertEquals(USER_TEST.getId(), project.getUserId());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(null, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(USER_TEST.getId(), null, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(USER_TEST.getId(), USER_PROJECT1.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(NON_EXISTING_USER_ID, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        @NotNull final Project project = service.changeProjectStatusById(USER_TEST.getId(), USER_PROJECT1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(project, service.findOneById(USER_TEST.getId(), USER_PROJECT1.getId()));
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        final int index = repository.findAll().indexOf(USER_PROJECT1);
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(null, index, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(USER_TEST.getId(), null, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(USER_TEST.getId(), index, null));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(USER_TEST.getId(), -1, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(NON_EXISTING_USER_ID, index, Status.COMPLETED));
        @NotNull final Project project = service.changeProjectStatusByIndex(USER_TEST.getId(), index, Status.COMPLETED);
        Assert.assertEquals(project, service.findOneByIndex(USER_TEST.getId(), index));
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void set() {
        @NotNull final IProjectService emptyService = new ProjectService(new ProjectRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(EMPTY_PROJECT_LIST, Collections.emptyList());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.set(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IProjectService emptyService = new ProjectService(new ProjectRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        Assert.assertEquals(USER_PROJECT_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findAll(""));
        Assert.assertEquals(Collections.emptyList(), service.findAll(NON_EXISTING_USER_ID));
        Assert.assertEquals(USER_PROJECT_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final IProjectService emptyService = new ProjectService(new ProjectRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.add(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_PROJECT_LIST, emptyService.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), comparator));
    }


    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(""));
        Assert.assertNull(service.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() {
        final int index = service.findAll().indexOf(USER_PROJECT1);
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(-1));
        @Nullable final Project project = service.findOneByIndex(index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = service.findAll().indexOf(USER_PROJECT1);
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(USER_PROJECT1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(null, index));
        @Nullable final Project project = service.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null, USER_PROJECT1.getId()));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void clear() {
        @NotNull final IProjectService emptyService = new ProjectService(new ProjectRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final IProjectService emptyService = new ProjectService(new ProjectRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(PROJECT_LIST);
        emptyService.removeAll();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeOne() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOne(null));
        @Nullable final Project project = service.add(ADMIN_PROJECT1);
        Assert.assertNotNull(service.findOneById(ADMIN_PROJECT1.getId()));
        service.removeOne(project);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null));
        Assert.assertNull(service.removeOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.add(ADMIN_PROJECT2);
        Assert.assertNotNull(service.findOneById(ADMIN_PROJECT2.getId()));
        service.removeOneById(project.getId());
        Assert.assertNull(service.findOneById(ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null, ADMIN_PROJECT2.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(ADMIN_TEST.getId(), null));
        Assert.assertNull(service.removeOneById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.add(ADMIN_PROJECT2);
        Assert.assertNotNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT2.getId()));
        service.removeOneById(project.getId());
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        service.add(ADMIN_PROJECT1);
        final int index = service.findAll().indexOf(ADMIN_PROJECT1);
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(-1));
        @Nullable final Project project = service.findOneByIndex(index);
        Assert.assertEquals(ADMIN_PROJECT1, project);
        Assert.assertNotNull(project);
        @Nullable final Project project2 = service.removeOneByIndex(index);
        Assert.assertNotNull(project2);
        Assert.assertEquals(project, project2);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        @Nullable final Project project = service.add(ADMIN_PROJECT1);
        final int index = service.findAll(ADMIN_TEST.getId()).indexOf(ADMIN_PROJECT1);
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(USER_PROJECT1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(null, index));
        @Nullable final Project project2 = service.removeOneByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(project2);
        Assert.assertEquals(project, project2);
        Assert.assertNull(service.findOneById(project2.getId(), project2.getUserId()));
    }

}
