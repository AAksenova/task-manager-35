package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.ITaskService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @Before
    public void before() {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> service.add(NULL_TASK));
        Assert.assertNotNull(service.add(ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task, service.findOneById(task.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, ADMIN_TASK1));
        Assert.assertThrows(AbstractException.class, () -> service.add("", ADMIN_TASK1));
        Assert.assertThrows(AbstractException.class, () -> service.add(ADMIN_TEST.getId(), NULL_TASK));
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void createByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.create(null, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(ADMIN_TEST.getId(), null, ADMIN_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), null));
        @NotNull final Task project = service.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertEquals(project, service.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), project.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void updateByUserIdById() {
        Assert.assertThrows(AbstractException.class, () -> service.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), null, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), USER_TASK1.getId(), null, USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(USER_TEST.getId(), USER_TASK1.getId(), USER_TASK1.getName(), null));
        @NotNull final Task task = service.updateById(USER_TEST.getId(), USER_TASK1.getId(), TASK_NAME, TASK_DESCR);
        Assert.assertEquals(task, service.findOneById(USER_TEST.getId(), task.getId()));
        Assert.assertEquals(TASK_NAME, task.getName());
        Assert.assertEquals(TASK_DESCR, task.getDescription());
        Assert.assertEquals(USER_TEST.getId(), task.getUserId());
    }

    @Test
    public void updateByUserIdByIndex() {
        final int index = repository.findAll().indexOf(USER_TASK1);
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(null, index, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), null, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), -1, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), index, null, USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(USER_TEST.getId(), index, USER_TASK1.getName(), null));
        @NotNull final Task task = service.updateByIndex(USER_TEST.getId(), index, TASK_NAME, TASK_DESCR);
        Assert.assertEquals(task, service.findOneByIndex(USER_TEST.getId(), index));
        Assert.assertEquals(TASK_NAME, task.getName());
        Assert.assertEquals(TASK_DESCR, task.getDescription());
        Assert.assertEquals(USER_TEST.getId(), task.getUserId());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(null, USER_TASK1.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(USER_TEST.getId(), null, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(USER_TEST.getId(), USER_TASK1.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(USER_TEST.getId(), NON_EXISTING_TASK_ID, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(NON_EXISTING_USER_ID, USER_TASK1.getId(), Status.IN_PROGRESS));
        @NotNull final Task task = service.changeTaskStatusById(USER_TEST.getId(), USER_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(task, service.findOneById(USER_TEST.getId(), USER_TASK1.getId()));
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        final int index = repository.findAll().indexOf(USER_TASK1);
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(null, index, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(USER_TEST.getId(), null, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(USER_TEST.getId(), index, null));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(USER_TEST.getId(), -1, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(NON_EXISTING_USER_ID, index, Status.COMPLETED));
        @NotNull final Task task = service.changeTaskStatusByIndex(USER_TEST.getId(), index, Status.COMPLETED);
        Assert.assertEquals(task, service.findOneByIndex(USER_TEST.getId(), index));
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void set() {
        @NotNull final ITaskService emptyService = new TaskService(new TaskRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(EMPTY_TASK_LIST, Collections.emptyList());
        emptyService.add(USER_TASK_LIST);
        emptyService.set(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ITaskService emptyService = new TaskService(new TaskRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_TASK_LIST);
        Assert.assertEquals(USER_TASK_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findAll(""));
        Assert.assertEquals(Collections.emptyList(), service.findAll(NON_EXISTING_USER_ID));
        Assert.assertEquals(USER_TASK_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final ITaskService emptyService = new TaskService(new TaskRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_TASK_LIST);
        emptyService.add(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_TASK_LIST, emptyService.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), comparator));
    }


    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(""));
        Assert.assertNull(service.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById("", USER_TASK1.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USER_TEST.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIndex() {
        final int index = service.findAll().indexOf(USER_TASK1);
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(-1));
        @Nullable final Task task = service.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = service.findAll().indexOf(USER_TASK1);
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(USER_TASK1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(null, index));
        @Nullable final Task task = service.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null, USER_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_TASK1.getId()));
    }

    @Test
    public void clear() {
        @NotNull final ITaskService emptyService = new TaskService(new TaskRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_TASK_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final ITaskService emptyService = new TaskService(new TaskRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(TASK_LIST);
        emptyService.removeAll();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeOne() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOne(null));
        @Nullable final Task task = service.add(ADMIN_TASK1);
        Assert.assertNotNull(service.findOneById(ADMIN_TASK1.getId()));
        service.removeOne(task);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null));
        Assert.assertNull(service.removeOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.add(ADMIN_TASK1);
        Assert.assertNotNull(service.findOneById(ADMIN_TASK1.getId()));
        service.removeOneById(task.getId());
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null, ADMIN_TASK2.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(ADMIN_TEST.getId(), null));
        Assert.assertNull(service.removeOneById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.add(ADMIN_TASK2);
        Assert.assertNotNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_TASK2.getId()));
        service.removeOneById(task.getId());
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        service.add(ADMIN_TASK1);
        final int index = service.findAll().indexOf(ADMIN_TASK1);
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(-1));
        @Nullable final Task task = service.findOneByIndex(index);
        Assert.assertEquals(ADMIN_TASK1, task);
        Assert.assertNotNull(task);
        @Nullable final Task task2 = service.removeOneByIndex(index);
        Assert.assertNotNull(task2);
        Assert.assertEquals(task, task2);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        @Nullable final Task task = service.add(ADMIN_TASK1);
        final int index = service.findAll(ADMIN_TEST.getId()).indexOf(ADMIN_TASK1);
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(ADMIN_TASK1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(null, index));
        @Nullable final Task task2 = service.removeOneByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(task2);
        Assert.assertEquals(task, task2);
        Assert.assertNull(service.findOneById(task2.getId(), task2.getUserId()));
    }

}
