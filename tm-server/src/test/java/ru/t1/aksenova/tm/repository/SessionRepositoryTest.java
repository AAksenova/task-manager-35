package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Session;

import java.util.Collections;

import static ru.t1.aksenova.tm.constant.SessionTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @Before
    public void before() {
        repository.add(USER_TEST.getId(), USER_SESSION_TEST);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_SESSION_TEST));
        @Nullable final Session session = repository.findOneById(ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST, session);
    }

    @Test
    public void findAll() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(SESSION_LIST);
        Assert.assertEquals(SESSION_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_SESSIONS_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST, session);
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USER_TEST.getId(), USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST, session);
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_SESSION_TEST.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_SESSION_TEST.getId()));
    }

    @Test
    public void clear() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(SESSION_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(SESSION_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Session session = repository.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
        repository.removeOne(session);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
        repository.removeOneById(session.getId());
        Assert.assertNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(ADMIN_SESSION_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_SESSION_TEST.getId()));
        repository.add(ADMIN_SESSION_TEST);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION_TEST.getId(), ADMIN_SESSION_TEST.getId()));
    }

}
