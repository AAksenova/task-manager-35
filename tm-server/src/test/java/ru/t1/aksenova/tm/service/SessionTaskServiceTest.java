package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.api.service.ISessionService;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.repository.SessionRepository;

import java.util.Collections;

import static ru.t1.aksenova.tm.constant.SessionTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionTaskServiceTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @NotNull
    private final ISessionService service = new SessionService(repository);

    @Before
    public void before() {
        repository.add(USER_SESSION_TEST);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> service.add(NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_SESSION_TEST));
        @Nullable final Session session = service.findOneById(ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST, session);
    }

    @Test
    public void addMany() {
        @NotNull final ISessionService emptyService = new SessionService(new SessionRepository());
        Assert.assertNotNull(emptyService.add(SESSION_LIST));
        for (final Session session : SESSION_LIST)
            Assert.assertEquals(session, emptyService.findOneById(session.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, ADMIN_SESSION_TEST));
        Assert.assertThrows(AbstractException.class, () -> service.add("", ADMIN_SESSION_TEST));
        Assert.assertThrows(AbstractException.class, () -> service.add(ADMIN_TEST.getId(), NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_SESSION_TEST));
        @Nullable final Session session = service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST, session);
    }

    @Test
    public void set() {
        @NotNull final ISessionService emptyService = new SessionService(new SessionRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSIONS_LIST);
        emptyService.set(SESSION_LIST);
        Assert.assertEquals(SESSION_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ISessionService emptyService = new SessionService(new SessionRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(SESSION_LIST);
        Assert.assertEquals(SESSION_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findAll(""));
        Assert.assertEquals(Collections.emptyList(), service.findAll(NON_EXISTING_SESSION_ID));
        Assert.assertEquals(USER_SESSIONS_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(""));
        Assert.assertNull(service.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST, session);
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById("", USER_SESSION_TEST.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USER_TEST.getId(), USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST, session);
    }


    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_SESSION_TEST.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null, USER_SESSION_TEST.getId()));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_SESSION_TEST.getId()));
    }

    @Test
    public void clear() {
        @NotNull final ISessionService emptyService = new SessionService(new SessionRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(SESSION_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final ISessionService emptyService = new SessionService(new SessionRepository());
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(SESSION_LIST);
        emptyService.removeAll();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeOne() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOne(null));
        @Nullable final Session session = service.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
        service.removeOne(session);
        Assert.assertNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null));
        Assert.assertNull(service.removeOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
        service.removeOneById(session.getId());
        Assert.assertNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null, ADMIN_SESSION_TEST.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(ADMIN_TEST.getId(), null));
        Assert.assertNull(service.removeOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION_TEST.getId()));
        service.removeOneById(session.getId());
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION_TEST.getId()));
    }

}
