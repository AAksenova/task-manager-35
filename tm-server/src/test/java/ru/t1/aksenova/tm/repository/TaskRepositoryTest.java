package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.aksenova.tm.constant.ProjectTestData.USER_PROJECT2;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @Before
    public void before() {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task, repository.findOneById(task.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertNull(repository.add(null, ADMIN_TASK1));
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);

    }

    @Test
    public void createByUserId() {
        @NotNull final Task task = repository.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertEquals(task, repository.findOneById(ADMIN_TEST.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId());
    }

    @Test
    public void set() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.set(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        Assert.assertEquals(USER_TASK_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_TASK_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.add(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_TASK_LIST, emptyRepository.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()), repository.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TEST.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIndex() {
        final int index = repository.findAll().indexOf(USER_TASK1);
        @Nullable final Task task = repository.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = repository.findAll().indexOf(USER_TASK1);
        @Nullable final Task task = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_TASK1.getId()));
    }

    @Test
    public void clear() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(TASK_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Task task = repository.add(ADMIN_TASK1);
        Assert.assertNotNull(repository.findOneById(ADMIN_TASK1.getId()));
        repository.removeOne(task);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.add(ADMIN_TASK2);
        Assert.assertNotNull(repository.findOneById(ADMIN_TASK2.getId()));
        repository.removeOneById(task.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(ADMIN_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_TASK2.getId()));
        @Nullable final Task task = repository.add(ADMIN_TASK2);
        Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK2.getId()));
        repository.removeOneById(task.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        repository.add(ADMIN_TASK1);
        final int index = repository.findAll().indexOf(ADMIN_TASK1);
        @Nullable final Task task = repository.findOneByIndex(index);
        Assert.assertEquals(ADMIN_TASK1, task);
        Assert.assertNotNull(task);
        @Nullable final Task task2 = repository.removeOneByIndex(index);
        Assert.assertNotNull(task2);
        Assert.assertEquals(task, task2);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        @Nullable final Task task = repository.add(ADMIN_TASK1);
        final int index = repository.findAll(ADMIN_TEST.getId()).indexOf(ADMIN_TASK1);
        @Nullable final Task task2 = repository.removeOneByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(task2);
        Assert.assertEquals(task, task2);
        Assert.assertNull(repository.findOneById(task2.getId(), task2.getUserId()));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        @Nullable final List<Task> tasks = emptyRepository.findAllByProjectId(USER_TASK1.getUserId(), USER_PROJECT1.getId());
        Assert.assertEquals(USER_TASK_LIST, tasks);
        @Nullable final List<Task> tasks2 = emptyRepository.findAllByProjectId(USER_TASK1.getUserId(), USER_PROJECT2.getId());
        Assert.assertNotEquals(USER_TASK_LIST, tasks2);
    }

}
