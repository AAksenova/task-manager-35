package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @Before
    public void before() {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project, repository.findOneById(project.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertNull(repository.add(null, ADMIN_PROJECT1));
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);

    }

    @Test
    public void createByUserId() {
        @NotNull final Project project = repository.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(project, repository.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void set() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.set(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        Assert.assertEquals(USER_PROJECT_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_PROJECT_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.add(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_PROJECT_LIST, emptyRepository.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()), repository.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() {
        final int index = repository.findAll().indexOf(USER_PROJECT1);
        @Nullable final Project project = repository.findOneByIndex(index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = repository.findAll().indexOf(USER_PROJECT1);
        @Nullable final Project project = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void clear() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(PROJECT_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Project project = repository.add(ADMIN_PROJECT1);
        Assert.assertNotNull(repository.findOneById(ADMIN_PROJECT1.getId()));
        repository.removeOne(project);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.add(ADMIN_PROJECT2);
        Assert.assertNotNull(repository.findOneById(ADMIN_PROJECT2.getId()));
        repository.removeOneById(project.getId());
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(ADMIN_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_PROJECT2.getId()));
        @Nullable final Project project = repository.add(ADMIN_PROJECT2);
        Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT2.getId()));
        repository.removeOneById(project.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        repository.add(ADMIN_PROJECT1);
        final int index = repository.findAll().indexOf(ADMIN_PROJECT1);
        @Nullable final Project project = repository.findOneByIndex(index);
        Assert.assertEquals(ADMIN_PROJECT1, project);
        Assert.assertNotNull(project);
        @Nullable final Project project2 = repository.removeOneByIndex(index);
        Assert.assertNotNull(project2);
        Assert.assertEquals(project, project2);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        @Nullable final Project project = repository.add(ADMIN_PROJECT1);
        final int index = repository.findAll(ADMIN_TEST.getId()).indexOf(ADMIN_PROJECT1);
        @Nullable final Project project2 = repository.removeOneByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(project2);
        Assert.assertEquals(project, project2);
        Assert.assertNull(repository.findOneById(project2.getId(), project2.getUserId()));
    }

}
